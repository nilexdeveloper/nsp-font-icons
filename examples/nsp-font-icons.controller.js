(function() {
    'use strict';

    angular
        .module('NSPFontIcons')
        .controller('NSPFontIconsController', NSPFontIconsController);

    NSPFontIconsController.$inject = ['$scope', '$http'];

    /* @ngInject */
    function NSPFontIconsController($scope, $http) {
        var vm = this;
        vm.title = 'FontIcons';
        vm.icons = [];
        $http.get('examples/classes-model.json').success(function(data) {
           vm.icons = data;
        });
    }
})();